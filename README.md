# Help

Bears help functions

#### Verify purchase code
```
[GET] /api/v1/verify-purchase-code/{code}
```

#### Registered by purchase code
```
[GET] /api/v1/registered-by-purchase-code/{code}
```

#### Registered by domain
```
[GET] /api/v1/registered-by-domain/{domain}
```

#### Register by domain
```
[POST] /api/v1/register-domain-by-purchase-code
Post fields: purchase_code, domain
```