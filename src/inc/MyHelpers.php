<?php
namespace myclass\MyHelpers;
/**
 * Helpers
 * @since 1.0.0
 * 
 */

class MyHelpers {

    private $container;
    private $db;

    function __construct( $c ) {
        $this->container = $c;
        $this->db = $this->container->get( 'db' );
        return $this;
    }

    public function query_registered_by_purchase_code( $purchase_code ) {
        $result = $this->db->query('SELECT * FROM wp_verifytheme_domains where purchase_code="'. $purchase_code .'"')->fetchAll();
        
        return array(
            'count' => count( $result ),
            'result' => $result,
        );
    }

    public function query_registered_by_domain( $domain ) {
        $query_string = 'SELECT * FROM `wp_verifytheme_domains` where `server_name`=:domain';
        $prepared = $this->db->prepare( $query_string );
        $prepared->execute( array(
            ':domain' => $domain,
        ) );

        return $prepared->fetchAll();;
    }

    public function register_domain_by_purchase_code( $purchase_code = '', $domain = '', $publisher = 'envato' ) {
        $fields = array(
            /*
            '`purchase_code`'     => ':purchaseCode',
            '`publisher`'         => ':publisher',
            '`date_activated`'    => ':dateActivated',
            '`domain`'            => ':domain',
            '`status`'            => ':status',
            */
            '`purchase_code`'       => ':purchase_code',
            '`server_name`'         => ':server_name',
            '`status`'              => ':status',
        );

        $query_string = 'INSERT INTO purchase_code ('. implode( ', ', array_keys( $fields ) ) .') VALUES ('. implode( ", ", array_values( $fields ) ) .')';
        $prepared = $this->db->prepare( $query_string );
        $prepared->execute( array(
            /*
            ':purchaseCode' => $purchase_code,
            ':publisher' => $publisher,
            ':dateActivated' => date( 'Y-m-d' ),
            ':domain' => $domain,
            ':status' => 1
            */
            ':purchase_code'       => $purchase_code,
            ':server_name'         => $domain,
            ':status'              => 1,
        ) );

        return $this->db->lastInsertId();
    }

}
  