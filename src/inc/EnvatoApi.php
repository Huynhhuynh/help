<?php 
namespace myclass\EnvatoApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class EnvatoApi{

    protected $api_token = '';
	
	protected $client;

	public function __construct($api_token) {
		$this->api_token = $api_token;
		$this->client = new Client([
			'base_uri' => 'https://api.envato.com/',
			]);	
    }

    protected function GET($endpoint, $query = []){
		try {
			$response = $this->client->request('GET', $endpoint,[
				'query' => $query,
				'headers' => [
					'Authorization' => 'Bearer '.$this->api_token,
					],	
				]);
			return $this->checkResponse($response);
		} catch (ClientException $e) {
			/*
			echo '<h1>Error!!!</h1>';
			echo '<h3>Message:</h3>';
			echo $e->getMessage();
			echo '<h3>Request Uri:</h3>';
			echo $e->getRequest()->getUri();
			echo '<h3>Response body:</h3>';
			echo $e->getResponse()->getBody();
			*/
			return (object) array(
				'success' => false,
				'message' => $e->getMessage(),
			);
		}
    }
    
    protected function checkResponse($response) {
		$success = [200, 201];
		$statusCode = $response->getStatusCode();
		if(in_array($statusCode, $success)) {
			$data = json_decode($response->getBody());
			if(is_object($data) && isset($data->data)){
				$data = $data->data;
			}
			return $data;
		}
		return false;
	}

	/**
	 * Returns the details of a user's purchase identified by the purchase code
	 * @param  string $code The unique code of the purchase to return
	 * @return string json containing details about the purchase
	 */
	public function getBuyerPurchase ( $code ) {
		$query = ['code' => $code];
		return $this->GET( 'v3/market/author/sale', $query );
	}

}