<?php 
use Slim\Http\Request;
use Slim\Http\Response;

{
    /**
     * Routes App
     * @since 1.0.0
     * 
     */
    $app->get('/hello/{name}', function (Request $request, Response $response, array $args){
        $name = $args['name'];
        $response = $this->view->render($response, 'hello.php', array( 'name' => $name ));
        return $response;
    });
    
    $app->get('/', function(Request $request, Response $response, array $args) {
        $response = $this->view->render($response, 'default.php');
        return $response;
    });

    $app->get('/register-purchase-code', function(Request $request, Response $response, array $args) {
        $response = $this->view->render($response, 'register-purchase-code.php');
        return $response;
    });
}


{
    /**
     * Routes Api
     * @since 1.0.0
     * 
     */
    $app->get('/api/v1/verify-purchase-code/{code}', function(Request $request, Response $response, array $args){
        $purchase_code = $args['code'];
        $result = $this->envato_client->getBuyerPurchase( $purchase_code );
        return $response->withJson($result);
    });

    /**
     * Registered by purchase code
     * @since 1.0.0
     * 
     */
    $app->get('/api/v1/registered-by-purchase-code/{code}', function(Request $request, Response $response, array $args){
        $purchase_code = $args['code'];
        $result = $this->envato_client->getBuyerPurchase( $purchase_code );
        if( isset( $result->success ) && false == $result->success ) {
            return $response->withJson( $result );
        } else {
            return $response->withJson( $this->my_helpers->query_registered_by_purchase_code( $purchase_code ) );
        }
    });

    /**
     * Registered by domain
     * @since 1.0.0
     * 
     */
    $app->get('/api/v1/registered-by-domain[/{domain}]', function(Request $request, Response $response, array $args){
        $domain = $args['domain'];
        if( empty( $domain ) ) {
            return $response->withJson( array(
                'success' => false, 
                'message' => 'Error: Invalid data!',
            ) );
        } else {
            return $response->withJson( $this->my_helpers->query_registered_by_domain( $domain ) );
        }
    });

    /**
     * Purchase code register
     * @since 1.0.0
     */
    $app->post('/api/v1/register-domain-by-purchase-code', function(Request $request, Response $response ){
        $data = $request->getParsedBody();
        $fields = array_merge( array(
            'purchase_code' => '',
            'domain' => '',
        ), $data );

        if( empty( $fields['purchase_code'] ) || empty( $fields['domain'] ) ) {
            return $response->withJson( array(
                'success' => false,
                'message' => 'Error: Invalid data!',
            ) );
        }

        $result = $this->envato_client->getBuyerPurchase( $fields['purchase_code'] );

        if( isset( $result->success ) && false == $result->success ) {
            return $response->withJson( $result );
        } else {
            $registered = $this->my_helpers->query_registered_by_purchase_code( $fields['purchase_code'] );
           
            if( $registered['count'] > 0 ) {
                $search = array_search( trim($fields['domain'] ), array_column( $registered['result'], 'domain' ) );
                if( $search !== false ) {
                    $result = array(
                        'success' => true,
                        'message' => 'Domain is registered!',
                    );
                } else {
                    $register_domain = $this->my_helpers->register_domain_by_purchase_code( $fields['purchase_code'], $fields['domain'] );
                    $result = array(
                        'success' => true,
                        'message' => 'Domain registered successful! #' . $register_domain,
                    );
                }
            } else {
                $register_domain = $this->my_helpers->register_domain_by_purchase_code( $fields['purchase_code'], $fields['domain'] );
                $result = array(
                    'success' => true,
                    'message' => 'Domain registered successful! #' . $register_domain,
                );
            }

            return $response->withJson( $result );
        }
    });
}