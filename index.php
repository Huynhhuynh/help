<?php
use \myclass\EnvatoApi\EnvatoApi;

// composer vendor autoload
require 'src/vendor/autoload.php';

$config = require 'src/config.php';
$app = new \Slim\App( $config['config'] );

$container = $app->getContainer();

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['db'] = function ($c) {
    
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'], $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    
    return $pdo;
};

$container['view'] = new \Slim\Views\PhpRenderer('src/templates/');

$container['envato_client'] = new EnvatoApi( $config['envato_token'] );
$container['my_helpers'] = function( $c ) { 
    return new \myclass\MyHelpers\MyHelpers( $c );
};

$config = require 'src/routes.php';

$app->run();